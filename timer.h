/* **************************************************************************
 * Function: 定时器控制模块
 * Description: 定时器定义与控制
 ************************************************************************** */

#ifndef _TIMER_H
#define _TIMER_H

#include "macro.h"

/* 定时器编号 */
typedef enum {
	TIMER0 = 0,
	TIMER1,
	TIMER2,
	TIMER3,
} timer_num;

/* 定时器预分频值 */
enum timer_prescale_index {
	TIMER0_PRESCALE = 128,
	TIMER1_PRESCALE = 1,
	TIMER2_PRESCALE = 64,
	TIMER3_PRESCALE = 1,

	TIMER_PRESCALE_8BIT = 128, /* 控制步进电机的8位定时器预分频值 */
	TIMER_PRESCALE_16BIT = 1,  /* 控制步进电机的16位定时器预分频值 */
};

/* 定时器最大计数值 */
enum timer_count_max_index {
	TIMER0_COUNT_MAX = 0xFF,
	TIMER1_COUNT_MAX = 0xFFFF,
	TIMER2_COUNT_MAX = 0xFF,
	TIMER3_COUNT_MAX = 0xFFFF,
};

/* 定时器预分频对应寄存器设置值 */
enum timer0_prescaling_index {
	TIMER0_PRESCALE0 = 0,
	TIMER0_PRESCALE1,
	TIMER0_PRESCALE8,
	TIMER0_PRESCALE32,
	TIMER0_PRESCALE64,
	TIMER0_PRESCALE128,
	TIMER0_PRESCALE256,
	TIMER0_PRESCALE1024,
};

/* 定时器预分频对应寄存器设置值 */
enum timer1_prescaling_index {
	TIMER1_PRESCALE0 = 0,
	TIMER1_PRESCALE1,
	TIMER1_PRESCALE8,
	TIMER1_PRESCALE64,
	TIMER1_PRESCALE256,
	TIMER1_PRESCALE1024,
	TIMER1_FALLING,
	TIMER1_RISING,
};

/* 定时器预分频对应寄存器设置值 */
enum timer2_prescaling_index {
	TIMER2_PRESCALE0 = 0,
	TIMER2_PRESCALE1,
	TIMER2_PRESCALE8,
	TIMER2_PRESCALE64,
	TIMER2_PRESCALE256,
	TIMER2_PRESCALE1024,
	TIMER2_FALLING,
	TIMER2_RISING,
};

/* 定时器预分频对应寄存器设置值 */
enum timer3_prescaling_index {
	TIMER3_PRESCALE0 = 0,
	TIMER3_PRESCALE1,
	TIMER3_PRESCALE8,
	TIMER3_PRESCALE64,
	TIMER3_PRESCALE256,
	TIMER3_PRESCALE1024,
	TIMER3_FALLING,
	TIMER3_RISING,
};

#define TIMER_PRESCALE_8BIT  128 /* 控制步进电机的8位定时器预分频值 */
#define TIMER_PRESCALE_16BIT 1   /* 控制步进电机的16位定时器预分频值 */

/* 设置定时器比较值 */
#define SET_TIMER0_COMP_VALUE(n) (OCR0 = (u8)(n))
#define SET_TIMER1_COMP_VALUE(n) (OCR1A = (n))
#define SET_TIMER2_COMP_VALUE(n) (OCR2 = (u8)(n))
#define SET_TIMER3_COMP_VALUE(n) (OCR3A = (n))

/* 定时器2为计时时钟 */
#define TIMER2_FREQ 1000 /* timer2频率为1000Hz, 即每1ms中断1次 */
#define TIMER2_COMP_VALUE (u8)(FOSC / TIMER2_PRESCALE / TIMER2_FREQ - 1)
#define HZ TIMER2_FREQ   /* 每秒计数值 */

/* 系统jiffies计数, 每毫秒自增1 */
extern volatile u32 jiffies;


/* ************************** 对外接口函数 ************************** */

/* 计时变量初始化 */
#define TIME_INIT() static u32 base_time_variable
/* 标记开始时间 */
#define TIME_SET() base_time_variable = jiffies
/* 超时检测, 参数单位为毫秒 */
#define TIME_OUT(ms) jiffies - base_time_variable > ms ? true : false

/* 初始化全部定时器 */
void timer_init_all(void);

/* 设置定时器比较值 */
void set_timer_comp_value(timer_num num, u16 value);


#endif
