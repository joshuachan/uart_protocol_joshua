/* **************************************************************************
 * Function: 循环队列模块
 * Description: 实现循环队列的数据存取
 ************************************************************************** */

#ifndef _QUEUE_H
#define _QUEUE_H

#include "macro.h"

/* 定义队列结构 */
typedef struct {
	u8 *buf;                    /* 数据存储空间 */
	u8 buf_size;                /* 数据存储空间大小 */
	u8 data_index;              /* 当前数据起始位置 */
	u8 data_size;               /* 当前数据长度 */
} queue_st;


/* ************************** 对外接口函数 ************************** */

/* 队列非满, 存数据前需作此判断 */
bool queue_not_full(queue_st *q);

/* 队列非空, 取数据前需作此判断 */
bool queue_not_empty(queue_st *q);

/* 队列初始化 */
void queue_init(queue_st *q, u8 *buf, u8 buf_size);

/* 存数据, 需先判断队列非满 */
void queue_push(queue_st *q, u8 c);

/* 取数据, 需先判断队列非空 */
u8 queue_pop(queue_st *q);

/* 取出当前数据, 需先判断队列非空 */
u8 queue_get(queue_st *q);

/* 取指定位置数据, 不改变数据长度及读指针位置
 * @offset: 相对读指针的偏移, 不能超过当前数据长度
 */
u8 queue_get_index(queue_st *q, u8 offset);

/* 标记指定长度的数据已取出 */
void queue_mark_get(queue_st *q, u8 mark_len);

#endif
