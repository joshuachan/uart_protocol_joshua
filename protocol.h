#ifndef _PROTOCOL_H
#define _PROTOCOL_H

#include "macro.h"
#include "queue.h"

/* 协议定义:
 *
 * UART, 半双工, 9600或更高波特率
 * 8个数据位, 1个停止位, 无奇偶检验位
 * 字节之间的间隔小于1ms, 数据包之间的间隔大于10ms
 * 支持自动应答与重发机制
 *
 * 每个数据包为8字节, 格式如下:
 * [STX][cmd][num][data][data][data][data][checksum]
 *
 * STX: 起始标志
 * cmd: 命令
 * num: 命令对象编号
 * data: 数据(共4字节)
 * checksum: 检验值(协议包前7字节的补码和)
 */

 typedef struct {
	u8 stx;
	u8 cmd;
	u8 num;
	u8 data[4];
	u8 checksum;
} pack_st;

#define STX_FLAG '#' /* 起始标志 */
#define PACK_LEN 8   /* 协议包长度 */

/* 协议保留命令 */
enum cmd_reserved {
	CMD_ACK_VALID = 0xFA,          /* 收到有效包时的应答 */
	CMD_ACK_INVALID = 0xFB,        /* 收到无效包时的应答 */
	CMD_REQUEST_MASTER = 0xFC,     /* 主机申请 */
	CMD_ACK_REQUEST_MASTER = 0xFD, /* 同意主机申请时的应答 */
};


/* 接收字节之间最大间隔(ms), 超时此值则视为新的包 */
#define IDLE_BETWEEN_BYTES_MAX 2
/* 包之间最小间隔(ms), 超过此值才可以发送新包 */
#define IDLE_BETWEEN_PACKS_MIN 11
/* 应答超时上限(ms), 超时此值则视为无应答 */
#define TIME_OUT_ACk 6

/* 最大重发次数 */
#define MAX_RESEND_TIMES 3


/* ******************************* 接口函数 ******************************* */

/* 数据包发送, 在系统时钟定时器中执行 */
void isr_pack_send(void);

/* 编解码程序, 在uart接收中断中执行 */
void isr_codec(u8 data);

/* 获取接收的数据包结构, 失败返回NULL */
pack_st *pack_get(void);

#endif
