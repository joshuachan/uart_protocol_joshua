/* **************************************************************************
 * Function: 杂类硬件控制模块
 * Description: 杂类硬件(除开输入/输出/电机控制)定义与控制
 ************************************************************************** */

#ifndef _HARDWARE_H
#define _HARDWARE_H

#include <ina90.h>
#include <ioavr.h>
#include <intrinsics.h>
#include "macro.h"

/* 系统时钟 */
#define FOSC 16000000L

/* ********* 其它端口定义 ********* */

/* 直流电机控制/输出控制选通 */
#define DDR_CS_OTHER DDRD
#define PORT_CS_OTHER PORTD
#define CS_OTHER 6

/* CAN总线选通 */
#define CAN_CS (*((volatile u8 *)0x3FFF))

/* 工作状态指示LED */
#define DDR_LED DDRB
#define PORT_LED PORTB
#define LED 0


/* ************************** 外部接口函数 ************************** */

#define delay_s(t)  __delay_cycles(FOSC * (t))
#define delay_ms(t) __delay_cycles(FOSC / 1000 * (t))
#define delay_us(t) __delay_cycles(FOSC / 1000000 * (t))

/* 中断中用到的全局变量, 在其它函数内使用时, 一定要进入临界区,
 * 关闭中断, 函数结束时退出临界区, 开启中断
 */
#define CRITICAL_SECTION_START u8 __sreg = SREG; _CLI()
#define CRITICAL_SECTION_END   SREG = __sreg

/* 工作状态指示LED初始化 */
void led_init(void);

/* 工作状态指示LED闪烁 */
void led_run(void);

/* 基本端口初始化 */
void basic_port_init(void);

#endif
