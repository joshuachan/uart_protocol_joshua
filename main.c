/* **************************************************************************
 * Function: 程序入口
 * Description: 程序入口函数
 ************************************************************************** */

#include <stdio.h>
#include "uart.h"
#include "timer.h"
#include "hardware.h"
#include "protocol.h"
#include "macro.h"


/* 测试程序 */
void main(void)
{
	_CLI();
	led_init();                 /* 工作状态指示LED初始化 */
	uart_init(UART0, 38400);    /* UART0初始化 */
	uart_init(UART1, 38400);    /* UART1初始化 */
	timer_init_all();           /* 初始化全部定时器 */
	basic_port_init();          /* 基本端口初始化 */
	_SEI();                     /* 全局中断使能 */


	pack_st *tmp;
	DBG0("Sytem start!");

	/* 进入主循环 */
	while (true) {

		if ((tmp = pack_get()) != NULL) {
			if (tmp->cmd == 0x00) {
				uart_putstring(UART0, "Cmd system reset!");
				delay_s(2);
			} else {
				uart_putstring(UART0, "NULL,");
			}
		}

		_WDR();                 /* 复位WDT */
//		process_codec(UART0);   /* uart0通讯处理 */
//		process_codec(UART1);   /* uart1通讯处理 */
	}
}
