/* **************************************************************************
 * Function: UART I/O 功能模块
 * Description: 实现UART输入/回显/格式化输出功能
 ************************************************************************** */

#ifndef _UART_H
#define _UART_H

#include "macro.h"
#include "queue.h"

/* uart列表 */
typedef enum {
	UART0 = 0,
	UART1 = 1,

	UART_TOTAL, /* uart总数 */
} uart_num;

#define RX_BUF_SIZE 128         /* 接收缓冲区大小 */
#define TX_BUF_SIZE 128         /* 发送缓冲区大小 */


/* **************************** 函数接口 **************************** */

/* UART初始化 */
void uart_init(uart_num num, u32 baud);

/* 向外部传递接收队列 */
queue_st *get_rx_queue(uart_num num);

/* 读1字节, 无数据则忙等 */
u8 uart_getchar(uart_num num);

/* 尝试读1字节, 无数据返回0, 可用于检测按键 */
u8 uart_getkey(uart_num num);

/* 接收字符串, 并回显, 需在参数中给定字符串存放位置 */
void uart_getstring(uart_num num, u8 *str);

/* 接收整型数, 支持8进制/10进制/16进制输入 */
u16 uart_getnum(uart_num num);

/* 写1字节, 发送缓冲器满则忙等 */
void uart_putchar(uart_num num, u8 data);

/* 向uart0写1字节 */
#define uart0_putchar(data) uart_putchar(UART0, data)

/* 向uart0写1字节 */
#define uart0_putchar(data) uart_putchar(UART0, data)

/* 字符串输出
 * 连续的相同内容, 将只输出一次
 */
void uart_putstring(uart_num, u8 *str);

/* uart0字符串输出 */
#define uart0_putstring(str) uart_putstring(UART0, str)

/* uart1字符串输出 */
#define uart1_putstring(str) uart_putstring(UART1, str)

/* 格式化输出, 不支持浮点数 */
void uart_printf(uart_num, const u8 *fmt, ...);

/* uart0格式化输出 */
#define uart0_printf(fmt, ...) uart_printf(UART0, fmt, ##__VA_ARGS__)

/* uart1格式化输出 */
#define uart1_printf(fmt, ...) uart_printf(UART1, fmt, ##__VA_ARGS__)

/* 调试信息输出
 * DBG: 固定字符串输出
 * DBGF: 格式化字符串输出
 */
#ifdef DEBUG
#define DBG0(arg) uart0_putstring(arg)
#define DBGF0(fmt, ...) uart0_printf(fmt, ##__VA_ARGS__)
#else
#define DBG0(arg) (0)
#define DBGF0(...) (0)
#endif

#ifdef DEBUG
#define DBG1(arg) uart1_putstring(arg)
#define DBGF1(fmt, ...) uart1_printf(fmt, ##__VA_ARGS__)
#else
#define DBG1(arg) (0)
#define DBGF1(...) (0)
#endif

#endif
