/* **************************************************************************
 * Function: UART I/O 功能模块
 * Description: 实现UART输入/回显/格式化输出功能
 ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ioavr.h>
#include <intrinsics.h>
#include <comp_A90.h>
#include <avr_macros.h>
#include "uart.h"
#include "hardware.h"
#include "protocol.h"

static u8 rx_buf[UART_TOTAL][RX_BUF_SIZE];
static u8 tx_buf[UART_TOTAL][TX_BUF_SIZE];
static queue_st rx_queue[UART_TOTAL];
static queue_st tx_queue[UART_TOTAL];

/* 向外部传递接收队列 */
queue_st *get_rx_queue(uart_num num)
{
	return &rx_queue[num];
}

/* UART初始化 */
void uart_init(uart_num num, u32 baud)
{
	u16 ubrr;

	queue_init(&rx_queue[num], rx_buf[num], RX_BUF_SIZE);
	queue_init(&tx_queue[num], tx_buf[num], TX_BUF_SIZE);
	/* 设置baudrate */
	ubrr = (u16)((FOSC / (8 * baud)) - 1);

	if (num == UART0) {
		UBRR0H = (u8)(ubrr >> 8);
		UBRR0L = (u8)ubrr;
		/* 倍速发送 */
		UCSR0A |= 1 << U2X0;
		/* 设置帧格式: 8个数据位, 1个停止位 */
		UCSR0C = 3 << UCSZ00;
		/* 使能: 接收结束中断, 接收, 发送 */
		UCSR0B = (1<<RXCIE0) | (1<<RXEN0) | (1<<TXEN0);
	} else if (num == UART1) {
		UBRR1H = (u8)(ubrr >> 8);
		UBRR1L = (u8)ubrr;
		/* 倍速发送 */
		UCSR1A |= 1 << U2X1;
		/* 设置帧格式: 8个数据位, 1个停止位 */
		UCSR1C = 3 << UCSZ10;
		/* 使能: 接收结束中断, 接收, 发送 */
		UCSR1B = (1<<RXCIE1) | (1<<RXEN1) | (1<<TXEN1);
	}
}

/* uart0接收结束中断处理函数 */
#pragma vector = USART0_RXC_vect
__interrupt void uart0_rxc_isr_orig(void)
{
	u8 udr0 = UDR0;

	queue_push(&rx_queue[UART0], udr0);
	isr_codec(udr0);
}

/* uart1接收结束中断处理函数 */
#pragma vector = USART1_RXC_vect
__interrupt void uart1_rxc_isr_orig(void)
{
	u8 udr1 = UDR1;

	queue_push(&rx_queue[UART1], udr1);
}

/* uart0数据寄存器空中断处理函数 */
#pragma vector = USART0_UDRE_vect
__interrupt void uart0_udre_isr_orig(void)
{
	if (queue_not_empty(&tx_queue[UART0]))
		UDR0 = queue_pop(&tx_queue[UART0]);
	else
		UCSR0B &= ~(1 << UDRIE0);
}

/* uart1数据寄存器空中断处理函数 */
#pragma vector = USART1_UDRE_vect
__interrupt void uart1_udre_isr_orig(void)
{
	if (queue_not_empty(&tx_queue[UART1]))
		UDR1 = queue_pop(&tx_queue[UART1]);
	else
		UCSR1B &= ~(1 << UDRIE1);
}

/* 读1字节, 无数据则忙等 */
u8 uart_getchar(uart_num num)
{
	while (!queue_not_empty(&rx_queue[num]))
		_WDR();
	return queue_pop(&rx_queue[num]);
}

/* 尝试读1字节, 无数据返回0, 可用于检测按键 */
u8 uart_getkey(uart_num num)
{
	if (queue_not_empty(&rx_queue[num]))
		return queue_pop(&rx_queue[num]);
	else
		return 0;
}

/* 接收字符串, 并回显, 需在参数中给定字符串存放位置 */
void uart_getstring(uart_num num, u8 *str)
{
	u8 c;
	u8 *s = str;

	while ((c = uart_getchar(num)) != '\r') {
		if (c == '\b') {
			if ((str - s) > 0) {
				uart_putstring(num, "\b \b");
				str--;
			}
		} else {
			*str = c;
			str++;
			uart_putchar(num, c);
		}
	}
	*str = '\0';
	uart_putchar(num, '\n');
}

/* 接收整型数, 支持8进制/10进制/16进制输入 */
u16 uart_getnum(uart_num num)
{
	u8 str[10];

	uart_getstring(num, str);
	return (u16)strtol((char *)str, NULL, 0);
}

/* 写1字节, 发送缓冲器满则忙等 */
void uart_putchar(uart_num num, u8 data)
{
	while (!queue_not_full(&tx_queue[num]))
		;
	queue_push(&tx_queue[num], data);
#if 0
	if (data == '\n') {
		uart_putchar(num, '\r');
	}
#endif
	/* 使能中断, 以便自动发送 */
	if (num == UART0)
		UCSR0B |= (1 << UDRIE0);
	else if (num == UART1)
		UCSR1B |= (1 << UDRIE1);
}

/* 字符串输出
 * 连续的相同内容, 将只输出一次
 */
void uart_putstring(uart_num num, u8 *str)
{
	static u8 last_str[65] = {0};

	if (strncmp((char *)last_str, (char *)str, 64)) {
		strncpy((char *)last_str, (char *)str, 64);
		while (*str) {
			uart_putchar(num, *str++);
		}
	}
}

/* 格式化输出, 不支持浮点数 */
void uart_printf(uart_num num, const u8 *fmt, ...)
{
	u8 str[128];         /* 数组空间尽可能大一些, 以免越界 */
	va_list args;

	va_start(args, fmt);
	vsprintf((char *)str, (char *)fmt, args);
	uart_putstring(num, str);
	va_end(args);
}
