/* **************************************************************************
 * Function: 通用宏及类型定义
 * Description: 通用的位操作宏及数据类型定义
 ************************************************************************** */

#ifndef _MACRO_H
#define _MACRO_H

typedef unsigned char u8;
typedef unsigned int  u16;
typedef unsigned long u32;

typedef signed char s8;
typedef signed int  s16;
typedef signed long s32;

/* 从新定义bool类型 */
typedef enum {
	false = 0,
	true,
} bool;

/* 置位 */
#define BIT_SET(ADDRESS, BIT) ((ADDRESS) |= (1UL<<(BIT)))

/* 位清零 */
#define BIT_CLR(ADDRESS, BIT) ((ADDRESS) &= ~(1UL<<(BIT)))

/* 位检查 */
#define BIT_TST(ADDRESS, BIT) ((ADDRESS) & (1UL<<(BIT)))


#endif
