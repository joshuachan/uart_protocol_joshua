/* **************************************************************************
 * Function: 杂类硬件控制模块
 * Description: 杂类硬件(除开输入/输出/电机控制)定义与控制
 ************************************************************************** */

#include <ioavr.h>
#include <ina90.h>
#include <intrinsics.h>
#include <comp_A90.h>
#include <pgmspace.h>
#include "hardware.h"
#include "timer.h"
#include "uart.h"

/* 工作状态指示LED初始化 */
void led_init(void)
{
	BIT_SET(DDR_LED, LED);
}

/* 工作状态指示LED闪烁 */
void led_run(void)
{
	BIT_TST(PORT_LED, LED) ? BIT_CLR(PORT_LED, LED) : BIT_SET(PORT_LED, LED);
}

/* 基本端口初始化 */
void basic_port_init(void)
{
	/* 直流电机控制/输出控制选通 */
	BIT_SET(DDR_CS_OTHER, CS_OTHER);
	BIT_CLR(PORT_CS_OTHER, CS_OTHER);

	/* 外部存储器接口使能, 外部地址读写插入二个等待周期 */
	MCUCR |= (1 << SRE);
	MCUCR &= ~(1 << SRW10);
	XMCRA = (1<<SRW01) | (1<<SRW11);

	/* 端口C引脚全部作为高位地址, 禁止外部存储器总线保持使能 */
	XMCRB = 0x00;

#if 0
	/* A组端口为低位地址及数据线 */
	DDRA = 0x00;  /* 设置为输入 */
	PORTA = 0xFF; /* 上拉电阻使能 */

	/* C组端口为高位地址线 */
	DDRC = 0xFF;
	PORTC = 0xFF;

	/* 关闭WDT */
	WDTCR = (1<<WDCE) | (1<<WDE);
	WDTCR = 0x00;
#endif
}