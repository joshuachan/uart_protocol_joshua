/* **************************************************************************
 * Function: 定时器控制模块
 * Description: 定时器定义与控制
 ************************************************************************** */

#include <ioavr.h>
#include <ina90.h>
#include <intrinsics.h>
#include <comp_A90.h>
#include <pgmspace.h>
#include "timer.h"
#include "hardware.h"
#include "protocol.h"

volatile u32 jiffies; /* 系统jiffies计数, 每毫秒自增1 */

/* 定义timer0比较匹配中断处理函数 */
#pragma vector = TIMER0_COMP_vect
__interrupt void timer0_isr_orig(void)
{
//	timer0_isr();
}

/* 定义timer1比较匹配中断处理函数 */
#pragma vector = TIMER1_COMPA_vect
__interrupt void timer1_isr_orig(void)
{
//	timer1_isr();
}

/* 定义timer2比较匹配中断处理函数 */
#pragma vector = TIMER2_COMP_vect
__interrupt void timer2_isr_orig(void)
{
	TIME_INIT();

	jiffies++;
	/* 控制LED间隔0.5秒闪烁 */
	if (TIME_OUT(500)) {
		TIME_SET();
		led_run();
	}
	isr_pack_send();
}

/* 定义timer3比较匹配中断处理函数 */
#pragma vector = TIMER3_COMPA_vect
__interrupt void timer3_isr_orig(void)
{
//	timer3_isr();
}

/* timer0初始化 */
static void timer0_init(void)
{
	TIMSK &= ~(1 << OCIE0);
	TCCR0 = (1<<WGM01) | (TIMER0_PRESCALE128<<CS00);
	OCR0 = TIMER0_COUNT_MAX;
	TCNT0 = 0x00;
	TIMSK |= (1 << OCIE0);
}

/* timer1初始化 */
static void timer1_init(void)
{
	TIMSK &= ~(1 << OCIE1A);
	TCCR1A = 0x00;
	TCCR1B = (1<<WGM12) | (TIMER1_PRESCALE1<<CS10);
	OCR1A = TIMER1_COUNT_MAX;
	TCNT1 = 0x00;
	TIMSK |= (1 << OCIE1A);
}

/* timer2初始化 */
static void timer2_init(void)
{
	jiffies = 0;
	TIMSK &= ~(1 << OCIE2);
	TCCR2 = (1<<WGM21) | (TIMER2_PRESCALE64<<CS20);
	OCR2 = TIMER2_COMP_VALUE;
	TCNT2 = 0x00;
	TIMSK |= (1 << OCIE2);
}

/* timer3初始化 */
static void timer3_init(void)
{
	ETIMSK &= ~(1 << OCIE3A);
	TCCR3A = 0x00;
	TCCR3B = (1<<WGM32) | (TIMER3_PRESCALE1<<CS30);
	OCR3A = TIMER3_COUNT_MAX;
	TCNT3 = 0x00;
	ETIMSK |= (1 << OCIE3A);
}

/* 初始化全部定时器 */
void timer_init_all(void)
{
	timer0_init();
	timer1_init();
	timer2_init();
	timer3_init();
}

/* 设置定时器比较值 */
void set_timer_comp_value(timer_num num, u16 value)
{
	switch (num) {
	case TIMER0:
		SET_TIMER0_COMP_VALUE(value);
		break;
	case TIMER1:
		SET_TIMER1_COMP_VALUE(value);
		break;
	case TIMER2:
		SET_TIMER2_COMP_VALUE(value);
		break;
	case TIMER3:
		SET_TIMER3_COMP_VALUE(value);
		break;
	default:
		break;
	}
}
