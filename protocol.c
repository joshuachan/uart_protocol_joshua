#include <string.h>
#include "uart.h"
#include "timer.h"
#include "hardware.h"
#include "protocol.h"

static pack_st rx_pack;
static pack_st tx_pack;
static bool rx_pack_ready;
static bool tx_pack_ready;
static u8 resend_times;
static bool pack_send_complete;
static u32 time_last_byte;
static u32 time_last_pack;
static bool stx_found;

/* 检验码生成 */
static u8 checksum_gen(void *pack, u8 len)
{
	DBG0("checksum_gen,");
	u8 i;
	u8 ret;
	u8 *tmp;

	ret = 0;
	tmp = pack;
	for (i = 0; i < len; i++)
		ret += ~tmp[i];

	return ret;
}

/* 按协议格式填充发送缓冲 */
void pack_encode(u8 cmd, u8 num, u32 data)
{
	DBG0("pack_encode,");
	tx_pack.stx = STX_FLAG;
	tx_pack.cmd = cmd;
	tx_pack.num = num;
	memcpy(tx_pack.data, &data, DATA_LEN);
	tx_pack.checksum = checksum_gen(&tx_pack, PACK_LEN-1);
	tx_pack_ready = true;
	resend_times = 0;
	pack_send_complete = false;
}

/* 数据包发送, 在系统时钟定时器中执行 */
void isr_pack_send(void)
{
	u8 i;
	u8 *tmp;

	if (tx_pack_ready && jiffies - time_last_send > INTERVAL_BETWEEN_PACKS_MIN) {
		tmp = (u8 *)&tx_pack;
		for (i = 0; i < PACK_LEN; i++) {
			uart0_putchar(tmp[i]);
		}
		time_last_send = jiffies;
		tx_pack_ready = false;
	}
}

/* 包重发 */
static void pack_resend(void)
{
	DBG0("pack_resend,");
	if (!pack_send_complete && resend_times++ < MAX_RESEND_TIMES) {
		tx_pack_ready = true;
	}
}

/* 收到有效包的应答 */
static void ack_valid_pack(void)
{
	DBG0("ack_valid_pack,");
	pack_encode(CMD_ACK_VALID, NUM_ACK_VALID, DATA_ACK_VALID);
	tx_pack_ready = true;
}

/* 收到无效包的应答 */
static void ack_invalid_pack(void)
{
	DBG0("ack_invalid_pack,");
	pack_encode(CMD_ACK_INVALID, NUM_ACK_INVALID, DATA_ACK_INVALID);
	tx_pack_ready = true;
}

/* 解析传入的队列 */
static bool pack_decode(queue_st *q)
{
	u8 i;
	u8 *tmp;
	u8 checksum;
	bool ret;

	ret = false;
	tmp = (u8 *)&rx_pack;
	checksum = 0;       /* 每次接收需从新计算校验值 */

	CRITICAL_SECTION_START;
	/* 队列数据长度不可小于协议包长度 */
	while (q->data_size >= PACK_LEN) {
		/* 寻找包头标志 */
		if (queue_get(q) == STX_FLAG) {
			/* 填充接收缓冲区 */
			for (i = 0; i < PACK_LEN; i++)
				tmp[i] = queue_get_index(q, i);
			checksum = checksum_gen(tmp, PACK_LEN-1);
			/* checksum值比对 */
			if (checksum == tmp[7]) {
				/* 将已解析的协议包数据标记为已读 */
				queue_mark_get(q, PACK_LEN);
				/* 协议包已解码完成, 可供读取 */
				ret = true;
				break;
			} else {
				/* checksum不匹配, 抛弃此STX标志 */
				queue_pop(q);
			}
		} else {
			/* 队列数据开始位置不是包头标志, 抛弃之 */
			queue_pop(q);
		}
	}
	CRITICAL_SECTION_END;

	return ret;
}

/* 检查是否接收到协议包 */
static bool rx_pack_check(u8 data)
{
	bool ret;
	static u8 byte_count = 0;

	ret = false;
	if (jiffies - time_last_byte > IDLE_BETWEEN_BYTES_MAX) {
		/* 字节间超时, 视为新的包开始 */
		if (data == STX_FLAG) {
			byte_count = 1;
			stx_found = true;
		} else {
			byte_count = 0;
			stx_found = false;
		}
	} else {
		/* 间隔时间符合, 当前字节属于上一包 */
		if (stx_found) {
			byte_count++;
			if (byte_count >= PACK_LEN) {
				byte_count = 0;
				stx_found = false;
				ret = true;
			}
		} else if (data == STX_FLAG) {
			byte_count = 1;
			stx_found_flag = true;
		}
	}
	time_last_byte = jiffies; /* 记录最后字节接收时间 */

	return ret;
}

/* 命令预处理 */
static void cmd_preprocess(void)
{
	switch (rx_pack.cmd) {
	case CMD_ACK_VALID:
		pack_send_complete = true;
		break;
	case CMD_ACK_INVALID:
		pack_resend();
		break;
	case CMD_REQUEST_MASTER
	default:
		rx_pack_ready = true;
		ack_valid_pack();
		break;
	}
}

/* 编解码程序, 在uart接收中断中执行 */
void isr_codec(u8 data)
{
	if (rx_pack_check(data)) {
		/* 包格式正确 */
		if (pack_decode(get_rx_queue(UART0))) {
			/* 解析成功 */
			cmd_preprocess();
		} else {
			/* 解析失败 */
			ack_invalid_pack();
		}
	}
}

/* 获取接收的数据包结构, 失败返回NULL */
pack_st *pack_get(void)
{
	pack_st *ret;

	ret = NULL;
	if (rx_pack_ready) {
		rx_pack_ready = false;
		ret = &rx_pack;
	}

	return ret;
}
